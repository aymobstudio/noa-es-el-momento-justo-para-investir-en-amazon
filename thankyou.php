<!DOCTYPE html>
<html lang="es">

<head>
    <title>Gracias Por Su Solicitud</title>
    <link rel="icon" href="./images/phoenix-favicon.png" type="img" sizes="16x16">
    <link rel="stylesheet" href="css/thankyou.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M7LW664');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7LW664"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

    <div id="content">
        <div class="left_content fl">

            <div class="heading_div">
                <h1>Gracias Por Su Solicitud</h1>
                <span>Estás un paso más cerca de mejorar tu futuro financiero!<br/>Esto es lo que pasa después... </span>   

            </div>
            <div class="three_div_container">
                <div class="first_div full_div fl">
                    <div class="topheader first_header">
                        <div class="number_top">1</div>

                    </div>
                    <div class="mid first_header">
                        <h3>Pasemos A La Acción</h3>
                        Nuestro amable equipo pondrá en contacto su consulta con el asesor más adecuado cerca de usted.
                        </div>
                    </div>  
                <div class="mid_div full_div fl">
                    <div class="topheader mid_header">
                        <div class="number_top">2

                        </div>

                    </div>
                    <div class="mid first_header line_spacing">
                        <h2>Espera una llamada</h2>
                        Te enviaremos un texto y un correo electrónico de confirmación, además de una llamada de nuestros socios expertos de Financiable.
                    </div>
                </div>

                <div class="last_div full_div fl">
                    <div class="topheader last_header">
                        <div class="number_top">3</div>

                    </div>
                    <div class="mid first_header line_spacing2">
                        <h3>Puedes Relajarte</h3>
                        La tranquilidad de saber que sus finanzas han sido evaluadas profesionalmente
                    </div>
                </div>

                <div class="clear"></div>


            </div>
        </div>
        <div class="right_content fr">
            <div class="right_top_content">
                <div class="top_shild">
                    <img src="images/thankyou/animation_200_kssitijb.gif">
                </div>
                <div class="top_content">
                    <h3>Lo Que Debe Saber</h3>
                    <div class="content_box">
                       <div class="check fl"><img src="images/thankyou/check.png"></div> 
                       <div class="check_content fl">NO le pediremos ningún dato personal sensible, como su fecha de nacimiento o los datos de su cuenta bancaria.</div> 
                           <div class="clear"></div>
                    </div>
                    <div class="content_box">
                        <div class="check fl"><img src="images/thankyou/check.png"></div> 
                        <div class="check_content fl">Sus datos de contacto no se compartirán con terceros sin su permiso expreso.</div> 
                            <div class="clear"></div>
                     </div>
                     <div class="content_box">
                        <div class="check fl"><img src="images/thankyou/check.png"></div> 
                        <div class="check_content fl">Todos los asesores de nuestro panel están reconocidos y regulados por la Financial Conduct Authority.</div> 
                            <div class="clear"></div>
                     </div>

                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div id="background"></div>


</body>

</html>