// Register Modal

document.addEventListener('DOMContentLoaded', () => {
    let users = [{
            "name": "Leon",
            "country": "Spain",
            "ip": "193.103.230.139"
        },
        {
            "name": "Jonas",
            "country": "Spain",
            "ip": "	136.199.8.210"
        },
        {
            "name": "Emma",
            "country": "Spain",
            "ip": "136.172.206.3"
        },
        {
            "name": "Achim",
            "country": "Spain",
            "ip": "188.92.207.201"
        },
        {
            "name": "Paul",
            "country": "Spain",
            "ip": "136.172.240.165"
        },
        {
            "name": "Clara",
            "country": "Spain",
            "ip": "136.172.175.195"
        },
        {
            "name": "Maximilien",
            "country": "Spain",
            "ip": "	136.172.163.216"
        },
        {
            "name": "Sofia",
            "country": "Spain",
            "ip": "188.209.162.193"
        },
        {
            "name": "Elias",
            "country": "Spain",
            "ip": "136.199.168.191"
        },
        {
            "name": "Marie",
            "country": "Spain",
            "ip": "136.199.148.207"
        },
    ];

    let elem = document.createElement('div');
    elem.className = 'popup-register';
    document.body.appendChild(elem);
    let show = true;
    let indexUser = 0;
    let timerId;

    function startRegisterPopup() {
        timerId = setInterval(() => {
            if (show) {
                if (indexUser == users.length) indexUser = 0;
                let name = users[indexUser].name;
                let country = users[indexUser].country;
                let ip = users[indexUser].ip;
                if (ip != '') {
                    ip = ip.split('.')[3];
                } else ip = "X";
                document.querySelector('.popup-register').innerHTML = '<div class="user-info"><div class="name">' +
                    name + ' de <img src="./register-popup/images/' +
                    country + '.jpg"> se ha registrado hace <span class="time-ago">' +
                    getRandomIntInclusive(1, 5) + ' minutos</span></div><button class="btn-register" onclick="openModal()">Registrarse</button><div class="verified"><img class="tick_blue" src="./register-popup/images/tick_blue.png"> Registrante verificado <br /> (IP: X.X.X.' +
                    ip + ')</div></div>';
                elem.classList.add("show");
                show = false;
                indexUser++;
            } else {
                elem.classList.remove("show");
                show = true;
            }
        }, 5000);
    };

    elem.onmouseover = function () {
        clearInterval(timerId);
        elem.style = "margin-bottom:10px; ";
        show = false;
    };

    elem.onmouseout = function () {
        elem.style = "margin-bottom:0px;";
        startRegisterPopup();
    };

    startRegisterPopup();

    const regCustomCheckBox = document.getElementById("reg-checkbox-input");
    const regActionBtn = document.getElementById("reg-form-btn");
    regCustomCheckBox.addEventListener('click', function () {
        if (this.getAttribute("checked")) {
            regActionBtn.toggleAttribute("disabled");
        }
    });
});

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const modalTrigger = document.querySelectorAll('[data-modal]'),
    modal = document.querySelector('.reg-window'),
    modalCloseBtn = document.querySelector('[data-close]');

modalTrigger.forEach(btn => {
    btn.addEventListener('click', openModal);
});

function closeModal() {
    modal.classList.add('hide');
    modal.classList.remove('show');
    document.body.style.overflow = '';
}

function openModal() {
    modal.classList.add('show');
    modal.classList.remove('hide');
    document.body.style.overflow = 'hidden';
}

modalCloseBtn.addEventListener('click', closeModal);

modal.addEventListener('click', (e) => {
    if (e.target === modal) {
        closeModal();
    }
});

document.addEventListener('keydown', (e) => {
    if (e.code === "Escape" && modal.classList.contains('show')) {
        closeModal();
    }
});

const phoneInputField = document.getElementById("reg-phone");
const phoneInput = window.intlTelInput(phoneInputField, {
    initialCountry: "es",
    hiddenInput: "full_phone",
    geoIpLookup: function(success) {
        // Get your api-key at https://ipdata.co/
        fetch("https://api.ipdata.co/?api-key=fb6483095ab6d6ef96d67904a2d8fe9e05c462b3d7063c54f5024f89")
            .then(function(response) {
                if (!response.ok) return success("");
                return response.json();
            })
            .then(function(ipdata) {
                success(ipdata.country_code);
            });
    },
    utilsScript: "https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js?1613236686837", // just for formatting/placeholders etc
});


document.getElementById('reg-form-btn').addEventListener('click', function() {
    let code = phoneInput.getNumber();
    phoneInputField.value = code;
});

document.getElementById('reg-phone').addEventListener('keyup', function() {
    let phone = this.value;
    let phoneReg = /^[0-9]*$/;
    if (phone.length < 5 || !phoneReg.test(phone)) {
        document.getElementById("reg-phone-message").innerHTML = "Este campo no es valido.";
    } else {
        document.getElementById("reg-phone-message").innerHTML = " ";
        return true;
    }
});

const checkNameRegForm = function() {
    let fname = document.getElementById("reg-name").value;
    let regExp = fname.split(" ").length <= 2 ? new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})") : new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}\\s[a-zA-Z]{2,})");
    if ((fname.length > 32) || (fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 3)) {
        document.getElementById("reg-fname-message").innerHTML = "El campo de nombre completo no es válido.";
        return false;
    }
    if (fname.length > 32) {
        document.getElementById("reg-fname-message").innerHTML = "La longitud del nombre no debe exceder los 32 caracteres.";
        return false;
    }
    document.getElementById("reg-fname-message").innerHTML = " ";
    return true;
}

const checkEmailRegForm = function() {
    let email = document.getElementById("reg-email").value;
    const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if ((email.length < 2) || (!emailReg.test(email))) {
        document.getElementById("reg-email-message").innerHTML = "Este campo es obligatorio, ingrese un correo electrónico válido.";
    } else {
        document.getElementById("reg-email-message").innerHTML = " ";
        return true;
    }
}

const checkPasswordRegForm = function() {
    let pw = document.getElementById("reg-password").value;
    if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
        document.getElementById("reg-password-message").innerHTML = "La longitud de la contraseña debe tener al menos 8 caracteres. La contraseña debe contener al menos una letra, una letra mayúscula y un número.";
        return false;
    }
    if (pw.length > 15) {
        document.getElementById("reg-password-message").innerHTML = "La longitud de la contraseña no debe exceder los 15 caracteres.";
        return false;
    }
    document.getElementById("reg-password-message").innerHTML = " ";
    return true;
}