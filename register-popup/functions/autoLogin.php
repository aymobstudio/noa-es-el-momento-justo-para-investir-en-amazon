<?php

// Auto login to deposit page after registration
function autoLoginNewCustomer($token, $email){
    // auto login
    $chLogin = curl_init();

    curl_setopt($chLogin, CURLOPT_URL, 'https://marketrip.pandats-api.io/api/v3/system/loginToken');
    curl_setopt($chLogin, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($chLogin, CURLOPT_SSL_VERIFYPEER, 0); 
    curl_setopt($chLogin, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($chLogin, CURLOPT_POST, 1);
    curl_setopt($chLogin, CURLOPT_POSTFIELDS, "{\"email\":\"".$email."\"}");

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Accept: application/json';
    $headers[] = 'Authorization: Bearer '. $token;
    curl_setopt($chLogin, CURLOPT_HTTPHEADER, $headers);

    $resultLogin = curl_exec($chLogin);
    if (curl_errno($chLogin)) {
        return 'Error:' . curl_error($chLogin);
    }
    $reslog = json_decode($resultLogin);
    $depositPageUrl = $reslog->data->url;
    $url = $reslog->data->url;
    $hash = ltrim(strstr($url, '?'), '?');


    $logRes = [
        'reslog' => $reslog,
        'hash' => $hash
    ];
    return $logRes;

    curl_close($chLogin);
}