<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

function sendNewLeadEmailRegForm($funnel, $description, $firstname, $middlename, $lastname, $email, $password, $phone, $verificate_by_phone, $verificate_by_email, $fullReferral, $clickid, $userIp, $device, $country, $city, $postal, $finaltime, $page_url){
   // Instantiation and passing `true` enables exceptions
    $mail = new PHPMailer;

    try {
        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'in-v3.mailjet.com';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = '2e4f3457d06665b9f0cb630a1cd080be';      // SMTP username
        $mail->Password   = '14c2bde9de727b9625ee3d016e21eb99';      // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        //$mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

        //Recipients
        $mail->setFrom('gerry.m@metro-net.co', $funnel);
        $mail->addAddress('media@aymob.com');     // Add a recipient
        $mail->addAddress('ezmarketing111@gmail.com');     // Add a recipient
        $mail->addAddress('dan@aymob.com');     // Add a recipient
        $mail->addAddress('aymobanalytics@gmail.com');     // Add a recipient

        $base_url = $_SERVER['HTTP_REFERER'];

        function getTitle($url) {
            $data = file_get_contents($url);
            $title = preg_match('/<title[^>]*>(.*?)<\/title>/ims', $data, $matches) ? $matches[1] : null;
            return $title;
        }

        $page_title = getTitle($base_url);

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = "[New Lead] from $country | " . '=?UTF-8?B?'.base64_encode($page_title).'?=';
        $middlename=$middlename?$middlename.' ':'';
        
        $mail->Body   .= '<p><b>Full Name</b>: '.$firstname.' '.$middlename.''.$lastname.'</p><br>'.'<p><b>Email</b>: '.$email.'</p><br>'.'<p><b>Password</b>: '.$password.'</p><br>'.'<p><b>Number</b>: '.$phone.'</p><br>'.'<p><b>ClickID</b>: '.$clickid.'</p><br>'.'<p><b>Device</b>: '.$device.'</p><br>'.'<p><b>User IP</b>: '.$userIp.'</p><br>'.'<p><b>Country</b>: '.$country.'</p><br>'.'<p><b>City</b>: '.$city.'</p><br>'.'<p><b>Zip code</b>: '.$postal.'</p><br>'.'<p><b>Time to conversion =</b> '.$finaltime.'</p><br>'.'<p><b>Page URL:</b> '.$page_url.'</p>';
        // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        return $mail->send();
        echo 'Message has been sent';
    } catch (Exception $e) {
        echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
    }
}