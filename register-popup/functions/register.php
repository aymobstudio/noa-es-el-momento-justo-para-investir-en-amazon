<?php
function createNewCustomer($userIp, $firstname, $middlename, $lastname, $email, $password, $phone, $funnelName, $clickid, $description)
{
  if (isset($middlename)) {
    $lastname_crm = $middlename . '-' . $lastname;
  } else {
    $lastname_crm = $lastname;
  }

  $curl = curl_init();

  curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://platform.weleads.co/api/signup/procform',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS => '{
        "ai": "2958195",
        "ci": "1",
        "gi": "134",
        "userip": "' . $userIp . '",
        "firstname": "' . $firstname . '",
        "lastname": "' . $lastname_crm . '",
        "email": "' . $email . '",
        "password": "' . $password . '",
        "phone": "' . $phone . '",
        "so":"' . $funnelName . '",
        "MPC_1":"Lead Interested in investing in Amazon",
        "MPC_7":"' . $clickid . '",
        "MPC_8":"' . $description . '"
    }',
    CURLOPT_HTTPHEADER => array(
      'x-trackbox-username: NOA',
      'x-trackbox-password: Noam123!',
      'x-api-key: 2643889w34df345676ssdas323tgc738',
      'Content-Type: application/json'
    ),
  ));

  $response = curl_exec($curl);
  return json_decode($response, true);
  curl_close($curl);
}
