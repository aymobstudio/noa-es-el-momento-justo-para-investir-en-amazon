<?php
session_start();
require_once "./functions/register.php";
require_once "./functions/autoLogin.php";
require_once "./functions/newLeadEmailRegForm.php";
require_once "./functions/errorEmailRegForm.php";

require_once "./../--vendor/autoload.php";

$funnelName = "Support";
$description = 'Register Form';

$error = '';
$data = [
    'name' => '',
    'first_name' => '',
    'middle_name' => '',
    'last_name' => '',
    'email' => '',
    'password' => '',
    'phone' => '',
    'country' => '',
    'citizenship' => '',
    'currency' => '',
    'city' => '',
    'postalCode' => '',
    'birthday' => '',
    'first_name_err' => '',
    'last_name_err' => '',
    'email_err' => '',
    'password_err' => '',
    'phone_err' => '',
];


if($_SERVER['REQUEST_METHOD'] === 'POST'){
    //$ip_address = '140.134.26.139';
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

    if (isset($_POST['clickid'])) {
        $clickid = $_POST['clickid'];
    } else {
        $clickid = 'N/A';
    }
    
    if (isset($_POST['verificateby'])) {
        if ($_POST['verificateby'] === 'email') {
            $verificate_by_phone = 'N';
            $verificate_by_email = 'Y';
        } else {
            $verificate_by_phone = 'Y';
            $verificate_by_email = 'N';
        }
    }

    $data = [
        'name' => trim($_POST['fullname']),
        'first_name' => '',
        'middle_name' => '',
        'last_name' => '',
        'email' => trim($_POST['email']),
        'password' => trim($_POST['password']),
        'verification' => trim($_POST['verificateby']),
        'phone' => str_replace("+", "", $_POST['phone']),
        'full_phone' => $_POST['full_phone'],
        'device' => $_SERVER['HTTP_USER_AGENT'],

        'postal' => $_POST['postal'],
        'city' => $_POST['city'],
        'citizenship' => $_POST['citizenship'],
        'country' => $_POST['country'],
        'userip' => $_POST['userip'],

        'clickid' => $clickid,

        'first_name_err' => '',
        'last_name_err' => '',
        'email_err' => '',
        'password_err' => '',
        'phone_err' => '',
        'pass_length' => 8
    ];

    $referral = '';


    // validate firstname field
    if(1 === preg_match('~[0-9]~', $data['name'])){
        $data['first_name_err'] = 'Name shouldn\'t contain numbers!';
    } elseif(empty($data['name'])){
        $data['first_name_err'] = 'Field is empty!';
    }

    // First name & surname Only
    // $frstnameLastname = explode(" ", $data['name']);
    // $data['first_name'] = $frstnameLastname[0];
    // $data['last_name'] = $frstnameLastname[1];
    // if($frstnameLastname && sizeof($frstnameLastname) == 3){
    // $data['last_name'] .= " " .  $frstnameLastname[2];
    // }elseif($frstnameLastname && sizeof($frstnameLastname) == 4) {
    // $data['last_name'] .= " " .  $frstnameLastname[2] . " " . $frstnameLastname[3];
    // }
    // First name, middle name, surname
    $frstnameLastname = explode(" ", $data['name']);
    $data['first_name'] = $frstnameLastname[0];
    $data['last_name'] = $frstnameLastname[1];
    if($frstnameLastname && sizeof($frstnameLastname) >= 3){
    $data['first_name'] = $frstnameLastname[0];
    $data['middle_name'] = $frstnameLastname[1];
    $data['last_name'] = $frstnameLastname[2];
    }elseif($frstnameLastname && sizeof($frstnameLastname) == 4){
    $data['last_name'] .= " " .  $frstnameLastname[3];
    }elseif($frstnameLastname && sizeof($frstnameLastname) == 5) {
    $data['last_name'] .= " " .  $frstnameLastname[3] . " " . $frstnameLastname[4];
    }


    //validate email field
    if(filter_var($data['email'], FILTER_VALIDATE_EMAIL) == true){
        $data['email_err'] == '';
    } else {
        $data['email_err'] = 'Invalid Email!';
    }
    //validate password field
    if(empty($data['password'])){
        $data['password_err'] = 'Password field is empty!';
    // } elseif(!preg_match('/^\p{Xan}+$/', $data['password'])){
    //     $data['password_err'] = 'Password should contain only letters and numbers!';
    } elseif(strlen($data['password']) < $data['pass_length']){
        $data['password_err'] = 'Password must be at least '.$data['pass_length'].' characters long!';
    } elseif(!preg_match("#[0-9]+#", $data["password"])){
        $data['password_err'] = 'Password should contain at least one number!';
    } elseif( !preg_match("#[a-zA-Z]+#", $data["password"])){
        $data['password_err'] = 'Password should contain at least one letter!';
    } elseif( !preg_match("#[A-Z]+#", $data["password"])){
        $data['password_err'] = 'Password should contain at least one capital letter!';
    }
    // validate phone field
    if($data['phone'] == ''){
        $data['phone_err'] = 'Phone number field is empty!';
    }

    if (isset($_POST['initdate'])) {
        $curr_datetime = new DateTime();
        $finaltime = $curr_datetime->getTimestamp();
        $init_time = $_POST['initdate'];
        $finaltime -= (int)$init_time;
    }

    //Page URL
    $host  = $_SERVER['HTTP_HOST'];
    $req_uri   = rtrim(dirname(strtok($_SERVER["REQUEST_URI"], '?')), '/\\');
    $modified_uri = str_replace('register-popup', '', $req_uri);
    $page_url = "$host$modified_uri";

    // if error variables empty proceed with execution
    if(empty($data['first_name_err']) && empty($data['last_name_err']) && empty($data['email_err']) && empty($data['phone_err']) && empty($data['password_err'])){
        $finaltime = gmdate("H:i:s", $finaltime);
        // create new customer function call
        $register = createNewCustomer($data['userip'], $data['first_name'], $data['middle_name'] ? $data['middle_name'] : '', $data['last_name'], $data['email'], $data['password'], $data['full_phone'], $funnelName, $clickid, $description);
        sendNewLeadEmailRegForm($funnelName, $description, $data['first_name'], $data['middle_name'] ? $data['middle_name'] : '', $data['last_name'], $data['email'], $data['password'], $data['full_phone'], $verificate_by_phone, $verificate_by_email, $referral, $clickid, $data['userip'], $data['device'], $data['country'], $data['city'], $data['postal'], $finaltime, $page_url);
        if($register['data'] == 'Cant register lead, no more fallbacks available'){
            $error = 'Error trying to register, possible account duplicate.';
            echo $error;
            return;
        } elseif($register['data'] == 'Same user Exist in the last 20 min with the same IP. duplicate LEAD'){
            $error = 'Error trying to register, same user Exist in the last 20 min with the same IP.';
            echo $error;
            return;
        } else {
            // echo '<pre>';
            // var_dump($data);
            // echo '</pre>';
            // return;
            $_SESSION['loginUrl'] = $register['addonData']['data']['loginURL'];
            $extra = "thankyou.php?".$clickid;
            header("Location: //$host$modified_uri$extra");
        }
    }
}