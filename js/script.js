$(document).ready(function() {
    
    
  $("#btnSubmit").click(function(){
      var invest_val = $('#invest_input').val();
      $("#invest_result").val(invest_val * 5);

      if(invest_val == 250){
          $("#invest_result").val(invest_val * 2);
      } else if(invest_val > 250 && invest_val < 500){
          $("#invest_result").val(invest_val * 2.234);
      } else if(invest_val >= 500 && invest_val < 1000){
          $("#invest_result").val(invest_val * 3.734);
      } else if(invest_val >= 1000 && invest_val < 5000){
          $("#invest_result").val(invest_val * 5.234);
      } else if(invest_val >= 5000){
          $("#invest_result").val(invest_val * 5.734);
      } else if(invest_val < 250){
          alert('Minimum required capital of $250');
          //$("#invest_error").text('Minimum required capital of $250');
      }

      //console.log(invest_val);
  }); 
  
})

const regCustomCheckBox = document.getElementById("check");
const regActionBtn = document.getElementById("submitBtn");
regCustomCheckBox.addEventListener('click', function () {
    if (this.getAttribute("checked")) {
        regActionBtn.toggleAttribute("disabled");
    }
});

// const phoneInputFieldMf = document.getElementById("phone");
// const phoneInputMf = window.intlTelInput(phoneInputFieldMf, {
//     initialCountry: "es",
//     separateDialCode: true,
//     nationalMode: true,
//     hiddenInput: "full_phone",
//     geoIpLookup: function(callback) {
//         var country = $('#citizenship').val();
//         callback(country);
//     },
//     utilsScript: "intl-tel-input-master/intl-tel-input-master/build/js/utils.js?1603274336113"
// });


// document.getElementById('submitBtn').addEventListener('click', function() {
//     let code = phoneInputMf.getNumber();
//     phoneInputFieldMf.value = code;
// });

// document.getElementById('phone').addEventListener('keyup', function() {
//     let phone = this.value;
//     let phoneReg = /^[0-9]*$/;
//     if (phone.length < 5 || !phoneReg.test(phone)) {
//         document.getElementById("phone-message").innerHTML = "Este campo no es valido.";
//     } else {
//         document.getElementById("phone-message").innerHTML = " ";
//         return true;
//     }
// });

const checkName = function() {
    let fname = document.getElementById("name").value;
    let regExp = fname.split(" ").length <= 2 ? new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{2,})") : new RegExp("^([a-zA-Z]{2,}\\s[a-zA-Z]{1,}\\s[a-zA-Z]{2,})");
    if ((fname.length > 32) || (fname.length < 2) || (fname.search(/[0-9]/) > 0) || (!regExp.test(fname) && fname) || (fname.split(" ").length > 3)) {
        document.getElementById("fname-message").innerHTML = "El campo de nombre completo no es válido.";
        return false;
    }
    if (fname.length > 32) {
        document.getElementById("fname-message").innerHTML = "La longitud del nombre no debe exceder los 32 caracteres.";
        return false;
    }
    document.getElementById("fname-message").innerHTML = " ";
    return true;
}

const checkEmail = function() {
    let email = document.getElementById("email").value;
    const emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if ((email.length < 2) || (!emailReg.test(email))) {
        document.getElementById("email-message").innerHTML = "Este campo es obligatorio, ingrese un correo electrónico válido.";
    } else {
        document.getElementById("email-message").innerHTML = " ";
        return true;
    }
}

const checkPassword = function() {
    let pw = document.getElementById("password").value;
    if ((pw.length < 8) || (pw.search(/[a-z]/) < 0) || (pw.search(/[A-Z]/) < 0) || (pw.search(/[0-9]/) < 0)) {
        document.getElementById("password-message").innerHTML = "La longitud de la contraseña debe tener al menos 8 caracteres. La contraseña debe contener al menos una letra, una letra mayúscula y un número.";
        return false;
    }
    if (pw.length > 15) {
        document.getElementById("password-message").innerHTML = "La longitud de la contraseña no debe exceder los 15 caracteres.";
        return false;
    }
    document.getElementById("password-message").innerHTML = " ";
    return true;
}