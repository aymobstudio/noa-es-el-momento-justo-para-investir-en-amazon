<?php

require_once "geoip.php";
require_once "functions/register.php";
require_once "functions/autoLogin.php";
require_once "functions/newLeadEmail.php";
require_once "functions/errorEmail.php";

require_once "vendor/autoload.php";

$error = '';
$data = [
    'first_name' => '',
    'last_name' => '',
    'email' => '',
    'password' => '',
    'phone' => '',
    'country' => '',
    'citizenship' => '',
    'currency' => '',
    'city' => '',
    'postalCode' => '',
    'birthday' => '',
    'first_name_second' => '',
    'last_name_second' => '',
    'email_second' => '',
    'password_second' => '',
    'phone_second' => '',
    'country_second' => '',
    'citizenship_second' => '',
    'currency_second' => '',
    'city_second' => '',
    'postalCode_second' => '',
    'birthday_second' => '',
    'first_name_err' => '',
    'last_name_err' => '',
    'email_err' => '',
    'password_err' => '',
    'phone_err' => '',
    'first_name_second_err' => '',
    'last_name_second_err' => '',
    'email_second_err' => '',
    'password_second_err' => '',
    'phone_second_err' => ''
];
$funnelName = 'AMZES';
$description = 'El momento para Amazon';



// $partnerId = 60605;
// $secretKey = '046d1122c04e76bd76eb63e7f2abe2b5b842e6e9dacc8640657e9c4475027648';

$affiliate_data = [
    'partner_id' => 60605,
    'referal_id' => 'cx-35081_0_[AFP]',
    'secret_key' => '046d1122c04e76bd76eb63e7f2abe2b5b842e6e9dacc8640657e9c4475027648',
    'clientSource' => 'GIH'
];


$_GET   = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);
if($_GET['affid']){
    if(preg_match("/^[A-Fa-f0-9]{5}$/", $_GET['affid']) > 0) {
        $affiliate_data['partner_id'] = $_GET['affid'];
        if($affiliate_data['partner_id'] == 44283) {
            $affiliate_data['partner_id'] = 44283;
            $affiliate_data['referal_id'] = 'cx-35091_0_[AFP]';
            $affiliate_data['secret_key'] = 'e8a67006b0f1fbcc6cc6fb61cc91e9712f59b720aa806b123a3589980447d4ea';
            $affiliate_data['clientSource'] = 'CXY';
        }
    }
}


$time = time();

$concatenated_string = $affiliate_data['partner_id'] . $time . $affiliate_data['secret_key'];
$accessKey= sha1($concatenated_string);

// panda authorisation
function pandaAuth($partnerId, $time, $accessKey){

    $ch = curl_init();
            
    curl_setopt($ch, CURLOPT_URL, 'https://marketrip.pandats-api.io/api/v3/authorization');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"partnerId\":\"".$partnerId."\",\"time\":\"".$time."\",\"accessKey\":\"".$accessKey."\"}");

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Accept: application/json';
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $result = curl_exec($ch);
    if (curl_errno($ch)) {
        $error = 'Error:' . curl_error($ch);
        die('Couldn\'t send request: ' . curl_error($ch));
    } 
    curl_close($ch);
    // $res = json_decode($result);
    // $jwtToken = $res->data->token;
    return $result;

}

if(isset($_POST['submit'])){
    if($_GET['clickid']){
        $clickID = $_GET['clickid'];
    }
    // Sanitize POST data
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $data = [
        'first_name' => trim($_POST['first_name']),
        'last_name' => trim($_POST['last_name']),
        'email' => trim($_POST['email']),
        'password' => trim($_POST['password']),
        'phone' => trim($_POST['phone']),
        'country' => strtolower(trim($_POST['country'])),
        'citizenship' => $_POST['citizenship'],
        'userip' => $_POST['userip'],
        'birthday' => '1980-01-01',
        'currency' => 'EUR',
        'city' => 'City',
        'postalCode' => '1234',
        'pass_length' => 6,
        'policy_check' => $_POST['terms'],
        'first_name_err' => '',
        'last_name_err' => '',
        'email_err' => '',
        'password_err' => '',
        'phone_err' => '',
        'customerExists' => '',
        'clientSource' => 'GIH',
        'referral' => 'partner_id=c1a486dd6c8f128d0be36f669aa221fe|PANDA_PARTNER='.$affiliate_data['partner_id'].'|referal_id='.$affiliate_data["referal_id"].'|affiliate_id=|password='.trim($_POST['password']).'|Description='.$description.'|Link=https://protradebtc.online/es/lp5/',
        'emailReferral' => 'partner_id=c1a486dd6c8f128d0be36f669aa221fe|PANDA_PARTNER='.$affiliate_data['partner_id'].'|referal_id='.$affiliate_data["referal_id"].'|affiliate_id=|password='.trim($_POST['password']).'|Description='.$description.'|link='.$_POST['fullUrl']
    ];

    // validate firstname field
    if(1 === preg_match('~[0-9]~', $data['first_name'])){
        $data['first_name_err'] = 'Name shouldn\'t contain numbers!';
    } elseif(empty($data['first_name'])){
        $data['first_name_err'] = 'Field is empty!';
        //$data['first_name_err'] = 'asdf';
    }
    // validate lastname field
    if(1 === preg_match('~[0-9]~', $data['last_name'])){
        $data['last_name_err'] = 'Last Name shouldn\'t contain numbers!';
    } elseif($data['last_name'] == ''){
        $data['last_name_err'] = 'Field is empty!';
    }
    //validate email field
    if(filter_var($data['email'], FILTER_VALIDATE_EMAIL) == true){
        $data['email_err'] == '';
    } else {
        $data['email_err'] = 'Invalid Email!';
    }
    //validate password field
    if(empty($data['password'])){
        $data['password_err'] = 'Password field is empty!';
    } elseif(!preg_match('/^\p{Xan}+$/', $data['password'])){
        $data['password_err'] = 'Password should contain only letters and numbers!';
    } elseif(strlen($data['password']) < $data['pass_length']){
        $data['password_err'] = 'Password must be at least 6 characters long!';
    } elseif(!preg_match("#[0-9]+#", $data["password"])){
        $data['password_err'] = 'Password should contain at least one number!';
    } elseif( !preg_match("#[a-zA-Z]+#", $data["password"])){
        $data['password_err'] = 'Password should contain at least one letter!';
    } 
    // validate phone field
    if($data['phone'] == ''){
        $data['phone_err'] = 'Phone number field is empty!';
    }



    // if error variables empty proceed with execution
    if(empty($data['first_name_err']) && empty($data['last_name_err']) && empty($data['email_err']) && empty($data['phone_err']) && empty($data['password_err']) && $data['policy_check'] === "on"){
       
        // Authorisation
        $authRes = json_decode(pandaAuth($affiliate_data['partner_id'], $time, $accessKey), true);

        if(!empty($authRes['error'])){

            errorEmail($funnelName, 'Auth Request Failed!', $authRes['error'][0]['code'], $authRes['error'][0]['description'], $authRes['requestId'], $data['first_name'], $data['last_name'], $data['email'], $data['password'], $data['phone'], $data['emailReferral']);
        }
        
        // create new customer function call
       $chLeadsResult = json_decode(createNewCustomer($authRes['data']['token'], $data['first_name'], $data['last_name'], $data['email'], $data['password'], $data['phone'], $data['country'], $data['citizenship'], $clickID, $data['birthday'], $data['currency'], $data['city'], $data['postalCode'], $affiliate_data['clientSource'], $data['referral']));
       
        $submitted = 'submitted';
       

        if(!empty($chLeadsResult->error)){
            foreach($chLeadsResult->error as $err){
                if($err->code == 'BL002' || $err->code == 'AH006'){
                    $data['customerExists'] = $err->description;
                } else if($err->code == 'RL000' || $err->code == 'WR001' || $err->code == 'RV009' || $err->code == 'WR000' || $err->code == 'BL003' || $err->code == 'BR000') {
                    errorEmail($funnelName, 'Registration Request Failed!', $err->code, $err->description, $chLeadsResult->requestId, $data['first_name'], $data['last_name'], $data['email'], $data['password'], $data['phone'], $data['emailReferral']);
                } 
            }
        } else {
            if($data['first_name'] !== 'test'){
                sendNewLeadEmail($funnelName, $data['first_name'], $data['last_name'], $data['email'], $data['phone'], $data['emailReferral']); 
            }
            
            // auto login
            $autologRes = autoLoginNewCustomer($authRes['data']['token'], $data['email']);
            header("Location: thankyou.php?".$autologRes['hash']);
            //exit();
        }
       
    }



}