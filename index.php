<?php 
$date = new DateTime();
$initdate = $date->getTimestamp();
?>
<?php require_once './register-popup/geoip.php'; ?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>El Momento Justo para Investir en Amazon</title>
    <link rel="icon" href="./images/phoenix-favicon.png" type="img" sizes="16x16">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;600;700&display=swap" rel="stylesheet">
    <!-- intl-intl -->
    <link rel="stylesheet" href="intl-tel-input-master/intl-tel-input-master/build/css/intlTelInput.css">
    <script src="intl-tel-input-master/intl-tel-input-master/build/js/intlTelInput.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="./register-popup/css/register.min.css">
    <!-- Google Tag Manager -->
    <script>
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start': new Date().getTime(),
                event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-M7LW664');
    </script>
    <!-- End Google Tag Manager -->
</head>

<body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M7LW664" height="0" width="0"
            style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <section id="hero">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h1>
                        El momento justo <br>para invertir en <br> <span>Amazon</span>
                    </h1>
                    <p>Benefíciese a largo plazo del crecimiento
                        de Amazon. Te mostraremos cómo.</p>
                    <!-- <a href="" class="btn">Ganancias estimadas</a> -->
                    <p><strong>Resgístrate y un representante<br>
                            te contactará</strong></p>
                </div>
                <div class="col-md-5">

                </div>
            </div>
        </div>
    </section>
    <section class="article-section">
        <div class="container">
            <div class="row flex-column-reverse flex-lg-row">
                <div class="col-lg-7 article">
                    <h3 class="font-weight-bold mb-3">Aumento del 468,11 % y sigue aumentado: ¡invierta en la imparable
                        Amazon!
                    </h3>
                    <p>
                        La imparable, Amazon (NASDAQ: AMZN), es la acción de comercio electrónico más buscada por los
                        inversores, y
                        hay una buena razón para esto. <b>Amazon ha hecho que inversores, como usted, se hagan
                            increíblemente ricos,
                            y sigue haciéndolo.</b> ¿Qué tan ricos? Si hace un tiempo hubiera invertido $10.000 en
                        Amazon, su fortuna
                        valdría más de <b>$12 millones.</b> Esto es una riqueza que cambia vidas.
                    </p>
                    <p>
                        Los expertos consideran que Amazon es la oportunidad de inversión más sobresaliente, <b>y
                            aseguran que
                            continuará creciendo.</b> Amazon es la tercera sociedad de cotización oficial más grande de
                        los mercados
                        de valores de Estados Unidos, valorada en $1,34 billones. Las acciones de Amazon, que superan la
                        referencia
                        de S&P 500, han visto aumentos significativos durante los últimos 5 años, <b>con un rápido
                            crecimiento
                            del 468,11 %</b> , haciendo muy felices a muchos inversores.
                    </p>
                    <div class="twitt-wrap">
                        <div class="twitt">
                            <div class="twitter-top">
                                <div class="twitter-person">
                                    <div class="twitter-name">
                                        <p class="name">KhalilHakim</p>
                                        <p class="tag">@TheKing_Hakim</p>
                                    </div>
                                </div>

                                <div class="twitter-logo">
                                </div>
                            </div>
                            <div class="twitter-body">
                                <p>Otros $3700 en un día. Gracias<span>#amzn</span></p>
                            </div>
                            <div class="twitter-top-footer">
                                <p>7:35 AM • Sep 3, 2020</p>
                            </div>
                            <div class="twitter-bottom-footer">
                                <div class="likes">
                                    <p>34</p>
                                </div>
                                <div class="other">
                                    <p>See KhalillHakim's other Tweets</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h3 class="mb-3 second-heading">¡No deje pasar esta oportunidad!</h3>
                    <p>
                        Si cree que el tren lo dejó, pregúntese: ¿puede imaginarse la vida sin Amazon? Gracias a la
                        COVID-19, Amazon
                        ha tenido que ver con casi todas las áreas de la vida diaria, incluso más debido a las
                        cuarentenas:
                        elementos para el hogar, abarrotes, streaming, juegos, artes, noticias y
                        entretenimiento. Incluso las web
                        que las personas visitan, como Netflix, Pinterest y Facebook funcionan gracias a que Amazon es
                        su proveedor
                        de computación en la nube.
                    </p>
                    <p>
                        Amazon declaró un ingreso de $75.000 millones en el primer trimestre de 2020 (aumentando desde
                        los $ 59.700
                        millones para el mismo periodo del año pasado). Las ventas de almacenes físicos, incluido Whole
                        Foods,
                        subieron a $ 4600 millones, y el negocio de computación en la nube llegó a la marca trimestral
                        de $ 10.000
                        millones por primera vez, un aumento del 33 % en un solo año.
                    </p>
                    <p>
                        No debe caber duda de esto. Amazon es imparable. Nada puede detener el avance de esta empresa
                        gigante. Amazon continuará interactuando con millones de personas de todo el mundo, todos los
                        días. Amazon
                        conservará su participación del mercado general y seguirá ofreciendo oportunidades de ensueño
                        para los
                        inversores.
                    </p>
                    <div class="text-center">
                        <img src="images/brands.jpg" class="img-fluid" alt="">
                    </div>
                </div>
                <div class="col-lg-5 sticky-wrap">
                    <div class="form-wrap sticky-form">
                        <div class="form-header">
                            <h5 class="text-center"><strong>Ahora es el momento justo para invertir en Amazon</strong>
                            </h5>
                        </div>
                        <div class="form-body">
                            <form id="r-form" action="reg-form.php" method="post">
                                <div id="r-error-box" class="text-danger font-weight-bold"></div>
                                <input type="hidden" id="initdate" name="initdate" value="<?php echo  $initdate; ?>">
                                <input type="hidden" id="country" name="country"
                                    value="<?php echo  $geo['country']; ?>">
                                <input type="hidden" id="city" name="city" value="<?php echo  $geo['city']; ?>">
                                <input type="hidden" id="postal" name="postal" value="<?php echo  $geo['postal']; ?>">
                                <input type="hidden" id="citizenship" name="citizenship"
                                    value="<?php echo  $geo['citizenship']; ?>">
                                <input type="hidden" id="userip" name="userip" value="<?php echo  $geo['ip']; ?>">
                                <input type="hidden" id="fullUrl" name="fullUrl" value="">
                                <div class="reg-input-wrapper mb-3 mt-3">
                                    <input class="form-control" id="name" name="fullname"
                                        placeholder="Nombre completo" type="text" maxlength="32" required
                                        onkeyup="checkName();">
                                    <div id="fname-message" class="text-danger font-weight-bold"></div>
                                </div>
                                <div class="reg-input-wrapper mb-3">
                                    <input class="form-control " id="email" name="email" placeholder="Email"
                                        type="email" required onkeyup="checkEmail();">
                                    <div id="email-message" class="text-danger font-weight-bold"></div>
                                </div>
                                <div class="reg-input-wrapper mb-3">
                                    <input class="form-control " id="password" name="password" placeholder="Contraseña"
                                        type="password" minlength="8" maxlength="15" required
                                        onkeyup="checkPassword();">
                                    <div id="password-message" class="text-danger font-weight-bold"></div>
                                </div>
                                <div class="reg-input-wrapper" id="phone-input-wrapper">
                                    <input id="phone" class="form-control int_tel" type="tel" name="phone" required>
                                    <div id="phone-message" class="text-danger font-weight-bold"></div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="check" checked="checked"
                                            name="terms" required>
                                        <label class="form-check-label" for="gridCheck">
                                            <small>Acepto los términos y condiciones.</small>
                                        </label>
                                    </div>
                                </div>
                                <div class="mb-3 text-center">
                                    <button class="" type="submit" id="submitBtn" name="submit">Comenzar »</button>
                                    <small>Debes ser mayor de 18 años para invertir en Amazon.
                                        Inversión minima requerida de £ 250.</small>
                                    <img src="images/dss-form.png" style="display:block; margin: 0 auto;" alt="">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="content">
        <img src="images/Vector-1-251x1024.png" alt="" class="waves">
        <div class="cards">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="box">
                            <img src="images/Group.png" alt="">
                            <h6>Seguro y protegido</h6>
                            <p>Siéntase seguro y protegido con una plataforma de inversión autorizada y una empresa en
                                la que puede
                                confiar.</p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box">
                            <img src="images/Group-2.png" alt="">
                            <h6>Fácil de usar</h6>
                            <p>Con experiencia o sin ella, nuestro equipo está disponible para ayudarlo y guiarlo con
                                sus inversiones.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box">
                            <img src="images/Group-1.png" alt="">
                            <h6>Pagos rápidos</h6>
                            <p>Las ganancias entrarán a tu cuenta en 24 horas. Sin tiempo de espera, sin formularios ni
                                apicaciones.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="calc">
            <div class="container">
                <h2>Descubre cuánta ganancias puedes ganar potencialmente</h2>
                <div class="calculator">
                    <form action="form_new.php" id="calculator">
                        <div class="row">
                            <div class="col-md-6 left">
                                <span>Inversión inicial</span>
                                <input type="text" id="invest_input" class="form-control" value="250">
                                <button id="btnSubmit" value="Calcular" type="button"
                                    class="submit_calc btn">Calcular</button>
                            </div>
                            <div class="col-md-6 right">
                                <span>Ganancias potenciales</span>
                                <div class="d-flex jusify-content-center">
                                    <span class="mb-0">€</span>
                                    <input type="text" class="form-control" id="invest_result">
                                </div>

                                <p>Máximo dos meses para comenzar a ver ganancias</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        <div class="steps">
            <div class="container">
                <h2>Comienza a invertir en 3 pasos fáciles</h2>
                <div class="row">
                    <div class="col-md-6 img">
                        <img src="images/Image-2-1024x682.jpg" alt="">
                    </div>
                    <div class="col-md-6 stp">
                        <div class="card mb-3">
                            <div class="row g-0">
                                <div class="col-md-1 d-flex justify-content-center align-items-center">
                                    <img src="images/1.png" alt="...">
                                </div>
                                <div class="col-md-11">
                                    <div class="card-body">
                                        <h6 class="card-title">Completa el formulario</h6>
                                        <p class="card-text">
                                            Deja tus datos personales y un Respresentante Autorizado te contactará para
                                            asistirte y responder
                                            todas tus preguntas.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-3">
                            <div class="row g-0">
                                <div class="col-md-1 d-flex justify-content-center align-items-center">
                                    <img src="images/2.png" alt="...">
                                </div>
                                <div class="col-md-11">
                                    <div class="card-body">
                                        <h6 class="card-title">Discute con nuestro equipo</h6>
                                        <p class="card-text">
                                            Una vez que se apruebe su cuenta, nuestro equipo estará allí para ayudarlo
                                            en cada paso del
                                            camino.

                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card mb-5">
                            <div class="row g-0">
                                <div class="col-md-1 d-flex justify-content-center align-items-center">
                                    <img src="images/3.png" alt="...">
                                </div>
                                <div class="col-md-11">
                                    <div class="card-body">
                                        <h6 class="card-title">Comienza a invertir</h6>
                                        <p class="card-text">
                                            Una vez que haya resuelto todas las consultas que pueda tener, ¡puede
                                            comenzar a invertir en su
                                            futuro financiero!
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="btn">Me interesa</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="slider">
            <h3 class="fw-bold text-center mb-5">Qué dicen los otros inversionistas</h3>
            <img src="images/testebef.png" alt="" class="hyphen">
            <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active"
                        aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1"
                        aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2"
                        aria-label="Slide 3"></button>
                    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="3"
                        aria-label="Slide 4"></button>
                </div>
                <div class="carousel-inner">

                    <div class="carousel-item active" data-bs-interval="10000">
                        <img src="images/Ellipse-6.png" class="d-block w-100" alt="...">
                        <div class="carousel-caption">
                            <p>He obtenido beneficios consecutivos desde que abrí mi cuenta. Con la pandemia del
                                coronavirus, las
                                cosas se pusieron más difíciles para mi familia, pero desde que comencé a invertir he
                                podido mantener a
                                mi familia a flote. ¡¡Gracias!!</p>
                            <h3 class="fw-bold">Sam Bennett</h3>
                            <h6 class="fw-bold">Irlanda</h6>
                        </div>
                    </div>
                    <div class="carousel-item " data-bs-interval="10000">
                        <img src="images/Ellipse-11.png" class="d-block w-100" alt="...">
                        <div class="carousel-caption">
                            <p>¡Esta es la mejor decisión que he tomado este año! Recibí mi primer pago después de
                                comenzar con una
                                pequeña cantidad. El personal fue extremadamente servicial y definitivamente invertiré
                                más</p>
                            <h3 class="fw-bold">Hayley Valentine</h3>
                            <h6 class="fw-bold">Isla de Wight</h6>
                        </div>
                    </div>
                    <div class="carousel-item " data-bs-interval="10000">
                        <img src="images/Ellipse-8.png" class="d-block w-100" alt="...">
                        <div class="carousel-caption">
                            <p>Después de que un amigo mío obtuvo ganancias constantes de la inversión, decidí
                                intentarlo. Con poco
                                conocimiento de inversión, encontré el proceso extremadamente simple y el gerente podía
                                ver las
                                ganancias justo después de abrir mi cuenta. ¡Se lo recomiendo a cualquiera que quiera
                                aumentar sus
                                fondos!</p>
                            <h3 class="fw-bold">Anthony Whittaker</h3>
                            <h6 class="fw-bold">Newcastle</h6>
                        </div>
                    </div>
                    <div class="carousel-item " data-bs-interval="10000">
                        <img src="images/Ellipse-12.png" class="d-block w-100" alt="...">
                        <div class="carousel-caption">
                            <p>Según las ganancias que he obtenido, el servicio que he recibido es lo que realmente me
                                inspiró a
                                seguir invirtiendo. Después de un año difícil, finalmente puedo decir que he encontrado
                                una plataforma
                                confiable y práctica para invertir y, en última instancia, ¡ver los retornos! </p>
                            <h3 class="fw-bold">Victoria Denton</h3>
                            <h6 class="fw-bold">Londres</h6>
                        </div>
                    </div>
                    <div class="carousel-item " data-bs-interval="10000">
                        <img src="images/Ellipse-12.png" class="d-block w-100" alt="...">
                        <div class="carousel-caption">
                            <p>Simple, confiable, escalable y consistente, ¡exactamente lo que estaba buscando! El
                                factor de confianza
                                fue enorme para mí y de ahora en adelante no invertiré con nadie más; todo se reduce al
                                sho con el que
                                está invirtiendo. ¡Tomé la decisión correcta! </p>
                            <h3 class="fw-bold">Jake Beischel</h3>
                            <h6 class="fw-bold">Yorkhore</h6>
                        </div>
                    </div>
                    <!-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark"  data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark"  data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button> -->
                </div>
            </div>
    </section>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <!-- <h6><strong>Descargo de responsabilidad de riesgo</strong></h6> -->
                    <p>ADVERTENCIA DE INVERSIÓN DE ALTO RIESGO: Los son instrumentos complejos y conllevan un alto
                        riesgo de
                        perder dinero rápidamente debido al apalancamiento. El 82% de las cuentas de inversores
                        minoristas pierden
                        dinero al negociar con este proveedor. Debe considerar si comprende cómo funcionan los y si
                        puede permitirse
                        asumir el alto riesgo de perder su dinero. Haga clic aquí para leer nuestra Divulgación de
                        riesgos. Phoenix
                        Markets es el nombre comercial de WGM Services Ltd, una empresa de servicios financieros sujeta
                        a la
                        regulación y supervisión de la Comisión de Bolsa de Valores de Chipre (CySEC) con el número de
                        licencia
                        203/13. Las oficinas de WGM Services Ltd se encuentran en 11, Vizantiou, 4th Floor, Strovolos
                        2064, Nicosia,
                        Chipre. </p>
                    <p>La información y los servicios contenidos en este sitio web no están destinados a residentes de
                        los Estados
                        Unidos o Canadá.</p>
                </div>
                <div class="col-md-4">
                    <a href="#" class="btn">Abrir una cuenta</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 border-top">
                    <span>© 2020 Todos los derechos reservados</span>

                </div>
                <div class="col-md-9">

                    <a href="https://algotechhub.info/es/lp5/privacy_policy.html"><span>Política de
                            privacidad</span></a>
                    <a href="https://algotechhub.info/es/lp5/terms_and_conditions.html"><span>Términos y
                            condiciones</span></a>
                </div>
            </div>
            <div class="row pl-0">
                <div class="col-12 addressCol">

                </div>
            </div>
        </div>
    </footer>

    <div class="reg-window">
        <header class="reg-window-header">
            <div class="row reg-window-header-inner">
                <div class="col-md-12 d-flex justify-content-between align-items-center">
                    <img class="reg-window-header-logo full-width" src="./register-popup/images/logo.png">
                    <button class="reg-window-close" data-close>&times;</button>
                </div>
            </div>
        </header>
        <div class="reg-window-inner d-flex justify-content-center align-items-center">
            <div class="reg-form d-flex justify-content-center align-items-center">
                <div class="container">
                    <div class="row d-flex justify-content-center align-items-center">
                        <div class="ml-md-auto col-md-5 reg-left">
                            <h2 class="reg-left-title">Empiece a invertir en acciones</h2>
                            <ul class="reg-list-items">
                                <li class="reg-list-item">
                                    <div class="reg-list-item-icon">
                                        <svg viewBox="0 0 50 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M37.5 50C37.2917 50 37.0833 49.9586 36.8875 49.8757C36.4042 49.6706 25 44.7323 25 34.5759V26.2527C25 25.5878 25.425 24.9974 26.0604 24.782L36.9979 21.0823C37.3229 20.9726 37.6771 20.9726 38.0021 21.0823L48.9396 24.782C49.575 24.9974 50 25.5878 50 26.2527V34.5759C50 44.7323 38.5958 49.6706 38.1125 49.8778C37.9167 49.9586 37.7083 50 37.5 50ZM28.125 27.3651V34.5738C28.125 41.478 35.325 45.6292 37.5 46.7229C39.675 45.6292 46.875 41.478 46.875 34.5738V27.3651L37.5 24.1937L28.125 27.3651Z"
                                                fill="#C9AB63"></path>
                                            <path
                                                d="M36.9232 41C36.4967 41 36.0831 40.8298 35.7815 40.52L31.4734 36.1563C30.8422 35.517 30.8422 34.4806 31.4734 33.8414C32.1045 33.2021 33.1277 33.2021 33.7589 33.8414L36.7509 36.8719L42.093 29.6544C42.623 28.9322 43.6397 28.7838 44.3548 29.3293C45.0679 29.8704 45.2122 30.8959 44.6758 31.6202L38.2135 40.3476C37.9335 40.7273 37.5048 40.9629 37.0352 40.9978C36.9986 40.9978 36.962 41 36.9232 41Z"
                                                fill="#C9AB63"></path>
                                            <path
                                                d="M22.8029 44H6.66177C3.53853 44 1 41.4166 1 38.2381V5.76191C1 2.58343 3.53853 0 6.66177 0H30.3382C33.4615 0 36 2.58343 36 5.76191V17.139C36 18.0065 35.3082 18.7105 34.4559 18.7105C33.6035 18.7105 32.9118 18.0065 32.9118 17.139V5.76191C32.9118 4.31829 31.7568 3.14286 30.3382 3.14286H6.66177C5.24324 3.14286 4.08824 4.31829 4.08824 5.76191V38.2381C4.08824 39.6817 5.24324 40.8571 6.66177 40.8571H22.8029C23.6553 40.8571 24.3471 41.5611 24.3471 42.4286C24.3471 43.296 23.6553 44 22.8029 44Z"
                                                fill="#C9AB63"></path>
                                            <path
                                                d="M27.4318 20H7.56818C6.70255 20 6 19.328 6 18.5C6 17.672 6.70255 17 7.56818 17H27.4318C28.2975 17 29 17.672 29 18.5C29 19.328 28.2975 20 27.4318 20Z"
                                                fill="#C9AB63"></path>
                                            <path
                                                d="M20.3929 28H8.60714C7.72 28 7 27.328 7 26.5C7 25.672 7.72 25 8.60714 25H20.3929C21.28 25 22 25.672 22 26.5C22 27.328 21.28 28 20.3929 28Z"
                                                fill="#C9AB63"></path>
                                            <path
                                                d="M17.375 11H7.625C6.728 11 6 10.328 6 9.5C6 8.672 6.728 8 7.625 8H17.375C18.272 8 19 8.672 19 9.5C19 10.328 18.272 11 17.375 11Z"
                                                fill="#C9AB63"></path>
                                        </svg>
                                    </div>
                                    <p class="reg-list-item-txt">
                                        Equilibrio, protección y mejores seguros
                                    </p>
                                </li>
                                <li class="reg-list-item">
                                    <div class="reg-list-item-icon">
                                        <img class="reg-gif-img" src="./register-popup/images/2.gif"
                                            alt="Safe, secure &amp; regulated trading environment">
                                    </div>
                                    <p class="reg-list-item-txt">
                                        Entorno de trading seguro, protegido y reglamentado
                                    </p>
                                </li>
                                <li class="reg-list-item">
                                    <div class="reg-list-item-icon">
                                        <svg viewBox="0 0 52 52" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M2.59161 27.001C3.28141 27.0021 3.94327 26.7358 4.43102 26.261C4.49863 26.1952 8.6594 22.1526 8.6594 22.1526L10.7944 24.2325C11.6073 25.0245 12.7103 25.4695 13.8604 25.4695C15.0105 25.4695 16.1134 25.0245 16.9263 24.2325L29.6444 11.8484L33.5122 15.6151C35.2066 17.2616 37.9506 17.2616 39.645 15.6151L47.3875 8.06833L48.0307 8.69441C48.6035 9.25142 49.4571 9.43274 50.2162 9.15868C50.9753 8.88461 51.501 8.20534 51.5621 7.41947L51.9955 1.98471V1.97627C52.0331 1.44344 51.8321 0.920832 51.4443 0.543016C51.0565 0.1652 50.5198 -0.0309002 49.9724 0.00521119L44.3839 0.427099C43.5768 0.483287 42.8778 0.993012 42.5954 1.73116C42.3131 2.4693 42.4996 3.29993 43.0724 3.8562L43.7156 4.48397L36.5773 11.4358L32.7104 7.66923C31.8975 6.87728 30.7945 6.43231 29.6444 6.43231C28.4943 6.43231 27.3914 6.87728 26.5784 7.66923L13.8604 20.0542L11.7271 17.9726C10.0329 16.3256 7.28846 16.3256 5.5943 17.9726L0.761737 22.6893C0.0190585 23.4106 -0.204013 24.4964 0.196565 25.4403C0.597144 26.3842 1.54246 27.0002 2.59161 27.001ZM1.98483 23.879L6.82086 19.1615C7.30862 18.6861 7.97051 18.419 8.6607 18.419C9.35089 18.419 10.0128 18.6861 10.5005 19.1615L13.2475 21.8371C13.4101 21.9955 13.6307 22.0845 13.8608 22.0845C14.0909 22.0845 14.3115 21.9955 14.4741 21.8371L27.8041 8.85979C28.8337 7.90212 30.4551 7.90212 31.4847 8.85979L35.3525 12.6264C36.0386 13.2643 37.1186 13.2643 37.8047 12.6264L45.555 5.07799C45.8928 4.74859 45.8928 4.21513 45.555 3.88574L44.2955 2.65973C44.2003 2.57369 44.1702 2.439 44.2201 2.32222C44.2638 2.19816 44.3811 2.1128 44.5157 2.10706L50.1007 1.6877C50.1443 1.68578 50.1867 1.70241 50.2168 1.73326C50.2511 1.76528 50.2695 1.81024 50.2671 1.85646L49.8337 7.29037C49.8242 7.41909 49.7385 7.53061 49.6143 7.57581C49.4901 7.621 49.3503 7.59157 49.2564 7.50047L48.0003 6.27615C47.6562 5.95952 47.1179 5.95952 46.7738 6.27615L38.4184 14.4211C37.3881 15.3761 35.7691 15.3761 34.7388 14.4211L30.871 10.6545C30.5465 10.3365 30.1048 10.1582 29.6444 10.1592C29.1848 10.1587 28.7439 10.3367 28.4196 10.6537L15.7015 23.0394C14.6717 23.9967 13.0507 23.9967 12.021 23.0394L9.88596 20.9595C9.20906 20.3013 8.11233 20.3013 7.43544 20.9595L3.20272 25.0729C2.86309 25.3965 2.32012 25.3965 1.9805 25.0729C1.81654 24.9179 1.72403 24.7048 1.72403 24.4823C1.72403 24.2598 1.81654 24.0467 1.9805 23.8917L1.98483 23.879Z"
                                                fill="#C9AB63"></path>
                                            <path
                                                d="M34 32.7618C34.0075 28.6441 31.2217 25.079 27.3113 24.2017C23.4008 23.3243 19.4253 25.3726 17.7658 29.1196C16.1062 32.8667 17.2124 37.2972 20.4218 39.7575H20.4005C19.3928 39.7529 18.4778 40.362 18.0689 41.3096C17.6599 42.2571 17.8345 43.3638 18.5136 44.1298C17.6296 45.1184 17.6296 46.6391 18.5136 47.6277C17.8345 48.3936 17.6599 49.5004 18.0689 50.4479C18.4778 51.3955 19.3928 52.0046 20.4005 52H30.6001C31.6079 52.0046 32.5228 51.3955 32.9318 50.4479C33.3407 49.5004 33.1662 48.3936 32.487 47.6277C33.3711 46.6391 33.3711 45.1184 32.487 44.1298C33.1662 43.3638 33.3407 42.2571 32.9318 41.3096C32.5228 40.362 31.6079 39.7529 30.6001 39.7575H30.5789C32.729 38.1123 33.9974 35.5187 34 32.7618ZM31.4501 45.8787C31.4501 46.3617 31.0695 46.7532 30.6001 46.7532H20.4005C19.9311 46.7532 19.5506 46.3617 19.5506 45.8787C19.5506 45.3958 19.9311 45.0043 20.4005 45.0043H30.6001C31.0695 45.0043 31.4501 45.3958 31.4501 45.8787ZM30.6001 50.2511H20.4005C19.9311 50.2511 19.5506 49.8595 19.5506 49.3766C19.5506 48.8936 19.9311 48.5021 20.4005 48.5021H30.6001C31.0695 48.5021 31.4501 48.8936 31.4501 49.3766C31.4501 49.8595 31.0695 50.2511 30.6001 50.2511ZM30.6001 41.5064C31.0695 41.5064 31.4501 41.8979 31.4501 42.3809C31.4501 42.8639 31.0695 43.2554 30.6001 43.2554H20.4005C19.9311 43.2554 19.5506 42.8639 19.5506 42.3809C19.5506 41.8979 19.9311 41.5064 20.4005 41.5064H30.6001ZM18.7006 32.7618C18.7006 28.8982 21.7449 25.7661 25.5003 25.7661C29.2557 25.7661 32.3001 28.8982 32.3001 32.7618C32.3001 36.6254 29.2557 39.7575 25.5003 39.7575C21.7467 39.7532 18.7048 36.6236 18.7006 32.7618Z"
                                                fill="#C9AB63"></path>
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M25.4992 31.75C25.0948 31.7496 24.7491 31.4881 24.6792 31.1298C24.6092 30.7714 24.8352 30.4195 25.2152 30.2951C25.5952 30.1708 26.0195 30.3099 26.2217 30.625C26.3656 30.8657 26.6436 31.0169 26.9473 31.0196C27.2511 31.0222 27.5323 30.876 27.6815 30.6379C27.8306 30.3997 27.824 30.1075 27.6642 29.875C27.3621 29.4187 26.8904 29.0734 26.3325 28.9V28.75C26.3325 28.3358 25.9594 28 25.4992 28C25.0389 28 24.6658 28.3358 24.6658 28.75V28.888C23.5187 29.2516 22.8302 30.3069 23.0363 31.3856C23.2424 32.4644 24.2829 33.2521 25.4992 33.25C25.9035 33.2504 26.2492 33.5119 26.3191 33.8702C26.3891 34.2286 26.1631 34.5805 25.7831 34.7049C25.4031 34.8292 24.9788 34.6901 24.7767 34.375C24.5397 34.0302 24.0412 33.9174 23.6511 34.1202C23.2611 34.323 23.1207 34.768 23.3341 35.125C23.6367 35.5815 24.109 35.927 24.6675 36.1V36.25C24.6675 36.6642 25.0406 37 25.5008 37C25.9611 37 26.3342 36.6642 26.3342 36.25V36.112C27.4816 35.7483 28.1701 34.6926 27.9636 33.6136C27.757 32.5347 26.7157 31.7472 25.4992 31.75Z"
                                                fill="#C9AB63"></path>
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M8.49916 38.75C8.09485 38.7496 7.74915 38.4881 7.6792 38.1298C7.60925 37.7714 7.83523 37.4195 8.21521 37.2951C8.59518 37.1708 9.01951 37.3099 9.22167 37.625C9.3656 37.8657 9.64364 38.0169 9.94734 38.0196C10.2511 38.0222 10.5323 37.876 10.6815 37.6379C10.8306 37.3997 10.824 37.1075 10.6642 36.875C10.3621 36.4187 9.89036 36.0734 9.33251 35.9V35.75C9.33251 35.3358 8.9594 35 8.49916 35C8.03892 35 7.66582 35.3358 7.66582 35.75V35.888C6.51868 36.2516 5.83021 37.3069 6.03631 38.3856C6.2424 39.4644 7.28295 40.2521 8.49916 40.25C8.90348 40.2504 9.24918 40.5119 9.31913 40.8702C9.38908 41.2286 9.16309 41.5805 8.78312 41.7049C8.40314 41.8292 7.97881 41.6901 7.77665 41.375C7.53966 41.0302 7.04115 40.9174 6.6511 41.1202C6.26106 41.323 6.12067 41.768 6.33413 42.125C6.63666 42.5815 7.10902 42.927 7.66748 43.1V43.25C7.66748 43.6642 8.04059 44 8.50083 44C8.96107 44 9.33417 43.6642 9.33417 43.25V43.112C10.4816 42.7483 11.1701 41.6926 10.9636 40.6136C10.757 39.5347 9.71569 38.7472 8.49916 38.75Z"
                                                fill="#C9AB63"></path>
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M43.4992 25.75C43.0948 25.7496 42.7491 25.4881 42.6792 25.1298C42.6092 24.7714 42.8352 24.4195 43.2152 24.2951C43.5952 24.1708 44.0195 24.3099 44.2217 24.625C44.3656 24.8657 44.6436 25.0169 44.9473 25.0196C45.2511 25.0222 45.5323 24.876 45.6815 24.6379C45.8306 24.3997 45.824 24.1075 45.6642 23.875C45.3621 23.4187 44.8904 23.0734 44.3325 22.9V22.75C44.3325 22.3358 43.9594 22 43.4992 22C43.0389 22 42.6658 22.3358 42.6658 22.75V22.888C41.5187 23.2516 40.8302 24.3069 41.0363 25.3856C41.2424 26.4644 42.2829 27.2521 43.4992 27.25C43.9035 27.2504 44.2492 27.5119 44.3191 27.8702C44.3891 28.2286 44.1631 28.5805 43.7831 28.7049C43.4031 28.8292 42.9788 28.6901 42.7767 28.375C42.5397 28.0302 42.0412 27.9174 41.6511 28.1202C41.2611 28.323 41.1207 28.768 41.3341 29.125C41.6367 29.5815 42.109 29.927 42.6675 30.1V30.25C42.6675 30.6642 43.0406 31 43.5008 31C43.9611 31 44.3342 30.6642 44.3342 30.25V30.112C45.4816 29.7483 46.1701 28.6926 45.9636 27.6136C45.757 26.5347 44.7157 25.7472 43.4992 25.75Z"
                                                fill="#C9AB63"></path>
                                            <path
                                                d="M43.4996 18C39.838 17.9958 36.5864 20.4088 35.4329 23.9862C34.2793 27.5636 35.4838 31.4993 38.421 33.75H38.3997C37.392 33.7454 36.477 34.3548 36.068 35.303C35.6591 36.2511 35.8336 37.3585 36.5128 38.125C35.6287 39.1142 35.6287 40.6358 36.5128 41.625C35.6287 42.6142 35.6287 44.1358 36.5128 45.125C35.6287 46.1142 35.6287 47.6358 36.5128 48.625C35.8336 49.3914 35.6591 50.4988 36.068 51.447C36.477 52.3951 37.392 53.0046 38.3997 53H48.5994C49.6072 53.0046 50.5222 52.3951 50.9311 51.447C51.34 50.4988 51.1655 49.3914 50.4864 48.625C51.3704 47.6358 51.3704 46.1142 50.4864 45.125C51.3704 44.1358 51.3704 42.6142 50.4864 41.625C51.3704 40.6358 51.3704 39.1142 50.4864 38.125C51.1655 37.3585 51.34 36.2511 50.9311 35.303C50.5222 34.3548 49.6072 33.7454 48.5994 33.75H48.5773C51.5155 31.5 52.7208 27.5639 51.5673 23.986C50.4138 20.4081 47.1615 17.995 43.4996 18ZM49.4494 39.875C49.4494 40.3582 49.0689 40.75 48.5994 40.75H38.3997C37.9303 40.75 37.5497 40.3582 37.5497 39.875C37.5497 39.3917 37.9303 39 38.3997 39H48.5994C49.0689 39 49.4494 39.3917 49.4494 39.875ZM49.4494 43.375C49.4494 43.8582 49.0689 44.25 48.5994 44.25H38.3997C37.9303 44.25 37.5497 43.8582 37.5497 43.375C37.5497 42.8917 37.9303 42.5 38.3997 42.5H48.5994C49.0689 42.5 49.4494 42.8917 49.4494 43.375ZM49.4494 46.875C49.4494 47.3582 49.0689 47.75 48.5994 47.75H38.3997C37.9303 47.75 37.5497 47.3582 37.5497 46.875C37.5497 46.3917 37.9303 46 38.3997 46H48.5994C49.0689 46 49.4494 46.3917 49.4494 46.875ZM48.5994 51.25H38.3997C37.9303 51.25 37.5497 50.8582 37.5497 50.375C37.5497 49.8917 37.9303 49.5 38.3997 49.5H48.5994C49.0689 49.5 49.4494 49.8917 49.4494 50.375C49.4494 50.8582 49.0689 51.25 48.5994 51.25ZM48.5994 35.5C49.0689 35.5 49.4494 35.8917 49.4494 36.375C49.4494 36.8582 49.0689 37.25 48.5994 37.25H38.3997C37.9303 37.25 37.5497 36.8582 37.5497 36.375C37.5497 35.8917 37.9303 35.5 38.3997 35.5H48.5994ZM43.4996 33.75C39.7441 33.75 36.6998 30.616 36.6998 26.75C36.6998 22.884 39.7441 19.75 43.4996 19.75C47.255 19.75 50.2994 22.884 50.2994 26.75C50.2952 30.6142 47.2532 33.7457 43.4996 33.75Z"
                                                fill="#C9AB63"></path>
                                            <path
                                                d="M3.42106 46.7541H3.39981C1.99157 46.7541 0.849974 47.9284 0.849974 49.377C0.849974 50.8257 1.99157 52 3.39981 52H13.5991C15.0074 52 16.149 50.8257 16.149 49.377C16.149 47.9284 15.0074 46.7541 13.5991 46.7541H13.5779C16.5154 44.5025 17.72 40.5681 16.5669 36.9913C15.4139 33.4145 12.1628 31 8.49948 31C4.83617 31 1.58502 33.4145 0.432008 36.9913C-0.721005 40.5681 0.483568 44.5025 3.42106 46.7541ZM13.5991 48.5027C14.0686 48.5027 14.4491 48.8942 14.4491 49.377C14.4491 49.8599 14.0686 50.2514 13.5991 50.2514H3.39981C2.9304 50.2514 2.54986 49.8599 2.54986 49.377C2.54986 48.8942 2.9304 48.5027 3.39981 48.5027H13.5991ZM8.49948 32.765C12.2548 32.765 15.299 35.8965 15.299 39.7595C15.299 43.6225 12.2548 46.7541 8.49948 46.7541C4.74418 46.7541 1.69992 43.6225 1.69992 39.7595C1.70413 35.8983 4.74593 32.7693 8.49948 32.765Z"
                                                fill="#C9AB63"></path>
                                        </svg>
                                    </div>
                                    <p class="reg-list-item-txt">
                                        Condiciones óptimas para maximizar su trading
                                    </p>
                                </li>
                            </ul>
                            <p class="reg-left-bottom-text">Al registrarse, recibirá correos electrónicos de marketing,
                                de los que
                                puede cancelar la suscripción en cualquier momento.</p>
                            <p class="reg-left-bottom-text">Todo comercio implica un riesgo. Solo capital de riesgo que
                                está dispuesto
                                a perder.</p>
                        </div>

                        <div class="col-md-7 reg-right">
                            <div class="reg-form-wrapper">
                                <h2 class="reg-form-title">Su oportunidad de invertir en acciones de Amazon</h2>
                                <form action="./register-popup/reg-form.php" method="post">
                                    <?php if (isset($_GET['clickid']) && ($_GET['clickid'] != "")) { ?>
                                    <input type="hidden" value="<?php echo $_GET['clickid']; ?>" name="clickid" />
                                    <?php  } ?>
                                    <input type="hidden" id="reg-initdate" name="initdate"
                                        value="<?php echo  $initdate; ?>">
                                    <input type="hidden" id="reg-country" name="country"
                                        value="<?php echo  $geo['country']; ?>">
                                    <input type="hidden" id="reg-city" name="city" value="<?php echo  $geo['city']; ?>">
                                    <input type="hidden" id="reg-postal" name="postal"
                                        value="<?php echo  $geo['postal']; ?>">
                                    <input type="hidden" id="reg-citizen" name="citizenship"
                                        value="<?php echo  $geo['citizenship']; ?>">
                                    <input type="hidden" id="reg-userip" name="userip"
                                        value="<?php echo  $geo['ip']; ?>">
                                    <div class="reg-input-wrapper">
                                        <input class="reg-input-field" id="reg-name" name="fullname"
                                            placeholder="Nombre completo" type="text" required
                                            onkeyup="checkNameRegForm();">
                                        <div id="reg-fname-message" class="reg-error-msg"></div>
                                    </div>
                                    <div class="reg-input-wrapper">
                                        <input class="reg-input-field " id="reg-email" name="email" placeholder="Email"
                                            type="email" required onkeyup="checkEmailRegForm();">
                                        <div id="reg-email-message" class="reg-error-msg"></div>
                                    </div>
                                    <div class="reg-input-wrapper">
                                        <input class="reg-input-field " id="reg-password" name="password"
                                            placeholder="Contraseña" type="password" required
                                            onkeyup="checkPasswordRegForm();">
                                        <div id="reg-password-message" class="reg-error-msg"></div>
                                    </div>
                                    <div class="reg-input-wrapper" id="reg-phone-input-wrapper">
                                        <input id="reg-phone" class="reg-input-field" type="tel" name="phone"
                                            placeholder="Número de teléfono" required>
                                        <div id="reg-phone-message" class="reg-error-msg"></div>
                                    </div>
                                    <label class="reg-checkbox-label" for="reg-checkbox-input">
                                        <input type="checkbox" id="reg-checkbox-input" class="reg-checkbox-input"
                                            checked="checked" />
                                        <span class="reg-checkmark"></span>
                                        <span class="reg-conditions">Acepto los Términos y Condiciones<br />Confirmo que
                                            tengo 18 años o
                                            más</span>
                                    </label>
                                    <button id="reg-form-btn">Empiece a invertir</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.6.0/dist/umd/popper.min.js"
        integrity="sha384-KsvD1yqQ1/1+IA7gi3P0tyJcT3vR+NdBTt13hSJ2lnve8agRGXTTyNaBYmCR/Nwi" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.min.js"
        integrity="sha384-nsg8ua9HAw1y0W1btsyWgBklPnCUAFLuTMS2G72MMONqmOymq585AcH49TLBQObG" crossorigin="anonymous">
    </script>

    <script src="register-popup/js/register-popup.js"></script>
    <script src="js/script.js"></script>

    <script>
        $(document).ready(function () {
            $("#fullUrl").val($(location).attr('href'));
        });
    </script>

    <script>
        $("#r-form").submit(function (e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            let form = $(this);
            let url = $(form).attr('action');

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                data: form.serialize(),
                beforeSend: function (xhr) {
                    $('#submitBtn').attr('disabled', true).html("Procesando...");
                },
                success: function (res) {
                    console.log('RES: ', res);
                    $('#submitBtn').attr('disabled', false).html("Comenzar »");;
                    if (res.status == 'false') {
                        $('#r-error-box').fadeIn().html(res.message);
                        setTimeout(function () {
                            $('#r-error-box').fadeOut().html();
                        }, 5000);
                    } else {
                        location.href = "thankyou.php";
                        console.log('Success');
                    }
                }
            });
        });
    </script>

    <script>
        const phoneInputFieldMf = document.getElementById("phone");
        const phoneInputMf = window.intlTelInput(phoneInputFieldMf, {
            initialCountry: "es",
            separateDialCode: true,
            nationalMode: true,
            hiddenInput: "full_phone",
            geoIpLookup: function (callback) {
                var country = $('#citizenship').val();
                callback(country);
            },
            utilsScript: "intl-tel-input-master/intl-tel-input-master/build/js/utils.js?1603274336113"
        });


        document.getElementById('submitBtn').addEventListener('click', function () {
            let code = phoneInputMf.getNumber();
            phoneInputFieldMf.value = code;
        });

        document.getElementById('phone').addEventListener('keyup', function () {
            let phone = this.value;
            let phoneReg = /^[0-9]*$/;
            if (phone.length < 5 || !phoneReg.test(phone)) {
                document.getElementById("phone-message").innerHTML = "Este campo no es valido.";
            } else {
                document.getElementById("phone-message").innerHTML = " ";
                return true;
            }
        });
    </script>

</body>

</html>